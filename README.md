# Fam's amazing dotfiles (for i3, and then some)
This repository contains a bunch of dotfiles and some scripts to quickly deploy an i3 environment with my working keybindings, layouts, rules and themes. This readme file is meant to be a rather complete guide on how to get everything up and running.

## What does the included link-dotfiles.sh script do?
Basically the script deletes current existing configs for the previously mentioned software and links the  files in this repo to those locations, as to keep them all in this centralised location.

## Software affected by this repo
The following config files WILL be deleted by the link-dotfiles.sh script:
- i3
- termite
- polybar
- micro
- compton
- rofi
- albert 
- tmux

## Software requirements
- i3-gaps
- termite
- polybar
- [openweathermap-detailed](https://github.com/x70b1/polybar-scripts/tree/master/polybar-scripts/openweathermap-detailed)
- micro
- compton
- rofi
- albert
- feh
- xfce4-clipman
- deepin-screenshot
- caffeine
- nm-applet
- brillo
- [lock.sh](https://gist.github.com/csivanich/10914698)

Additionally, the config file for i3 includes rules for the following apps:
- Firefox
- Franz
- Whatsie
- Messenger for Desktop
- Pamac
- Nautilus
- Totem

## Other requirements
### Fonts
- [Input](http://input.fontbureau.com/)
- [FontAwesome 4](https://fontawesome.com/v4.7.0/)
- DejaVu Sans
- [Weather Icons](https://erikflowers.github.io/weather-icons/)
- Noto Sans CJK

## Scripts in this repo
- launch.sh (polybar launch script)
- backlight-toggle.sh (backlight control script)

## Post-installation configs
### xinput
When i3 is first initialized mouse scrolling is not natural and tapping to click is disabled, this can be fixed by changing the corresponding values near the end of the i3 configuration file. These values are unique per device, and may even change for the same device after certain updates.

### Keyboard
Likewise, the keyboard defaults to the US layout, to use a different layout there's a line in the i3 config file specifying the setxkbmap command to achieve so, in this case the layout is set to US (AltGr Intl.)

### XFCE
XFCE must be told not to use its own compositor and window manager, and start i3 every session instead. This can be easily achieved through XFCE's own control centre.

## The future
In the future I plan to add custom syntax files for the micro editor for each dotfile included here
- (in progress) i3
- (in progress) termite
- (in progress) polybar
- (in progress) compton
- (in progress) rofi
- (in progress) albert
- (in progress) tmux