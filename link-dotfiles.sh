#!/bin/bash

# Simple script to link this collection of dotfiles to where they're supposed to be

CURRENT_DIR=$(pwd)

function link_stuff {
	# First we create the required directories
	mkdir -p $HOME/.config/i3
    mkdir -p $HOME/.config/termite
	mkdir -p $HOME/.config/polybar
	# compton places its config file in ~/.config
	mkdir -p $HOME/.config/rofi
	mkdir -p $HOME/.config/albert
	mkdir -p $HOME/.config/micro
	# tmux places its config file in ~

	# Now we link the dotfiles, hard links only, no items, final destination
    ln -f dotfiles/conf-tmux $HOME/.tmux
    ln -f dotfiles/conf-i3 $HOME/.config/i3/config

    ln -f dotfiles/conf-termite $HOME/.config/termite/config
    ln -f dotfiles/conf-polybar $HOME/.config/polybar/config
    ln -f dotfiles/conf-micro $HOME/.config/micro/settings.json

    ln -f dotfiles/conf-compton $HOME/.config/compton.conf

    ln -f dotfiles/conf-rofi $HOME/.config/rofi/config
    ln -f dotfiles/conf-albert $HOME/.config/albert/albert.conf

    # And now we link additional scripts

    # ln -f .config/polybar/launch.sh ~/.config/polybar/launch.sh
	# ln -f .aliases ~/.aliases
	# ln -f .tmux.conf ~/.tmux.conf
    # ln -f .fbtermrc ~/.fbtermrc
    # ln -f .nanorc ~/.nanorc
}

echo "MAKE SURE YOU'RE RUNNING THIS SCRIPT FROM THE ROOT OF THE REPO DIRECTORY TREE"
echo ""
echo "THIS WILL DELETE EXISTING CONFIGURATION FILES"
echo "THIS INCLUDES YOUR CURRENT .aliases FILE, MAKE SURE TO MAKE A BACKUP"

read -r -p "Are you sure? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY]) 
        link_stuff
        #echo boom
        ;;
    *)
        exit 0
        ;;
esac
